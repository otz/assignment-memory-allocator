
CC=gcc
CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG

BUILDDIR=build
SRCDIR=src

.PHONY: all clean test

all: $(BUILDDIR)/main.o $(BUILDDIR)/mem.o $(BUILDDIR)/util.o $(BUILDDIR)/tests.o
	$(CC) -o $(BUILDDIR)/main $^

build:
	mkdir -p $(BUILDDIR)

$(BUILDDIR)/%.o: $(SRCDIR)/%.c build
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)

test:
	@+cd tester; make CC=$(CC)
