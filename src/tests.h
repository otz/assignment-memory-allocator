#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H

#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>

#include "mem.h"

typedef bool test(va_list args);

bool run_test(FILE *out, const char *description, test *test, ...);

bool test_malloc1(va_list args);

bool test_free3_middle_first(va_list args);

bool test_free3_sequential(va_list args);

bool test_malloc2_with_allocation(va_list args);

bool test_malloc2_with_failproof_allocation(va_list args);

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
