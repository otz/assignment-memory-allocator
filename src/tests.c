#define _DEFAULT_SOURCE

#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <sys/mman.h>
#include <unistd.h>

#include "tests.h"

/* Helper functions */

bool run_test(FILE *out, const char *description, test *test, ...) {
    fprintf(out, "Running `%s`...\n", description);
    va_list args;
    va_start(args, test);
    const bool result = test(args);
    va_end(args);
    const char *const verdict = result ? "OK" : "FAILED";
    fprintf(out, "`%s`: %s\n", description, verdict);
    return result;
}

static bool test_ptr(void *ptr) {
    if (ptr == NULL) return false;
    *((volatile uint8_t *) ptr);
    return true;
}

static bool test_ptr3(void *restrict ptr1, void *restrict ptr2, void *restrict ptr3) {
    return test_ptr(ptr1) && test_ptr(ptr2) && test_ptr(ptr3);
}

static uint8_t test_mem_dummy_func(uint8_t x) { return 3 * x + 1; }

static bool test_mem_write_read(void *ptr, size_t size) {
    if (ptr == NULL) return false;
    volatile uint8_t *const mem = ptr;
    for (size_t i = 0; i < size; ++i) { mem[i] = test_mem_dummy_func(i); }
    for (size_t i = 0; i < size; ++i) { if (mem[i] != test_mem_dummy_func(i)) return false; }
    return true;
}

static bool test_mem2_write_read(void *restrict ptr1, size_t size1, void *restrict ptr2, size_t size2) {
    return test_mem_write_read(ptr1, size1) && test_mem_write_read(ptr2, size2);
}

static void *map_pages_exactly(const void *addr, size_t length) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
}

static void *map_free_pages_after(const void *page_addr, size_t length) {
    const size_t page_size = getpagesize();
    void *mapped;
    do {
        page_addr = (void *) ((uint8_t *) page_addr + page_size);
        mapped = map_pages_exactly(page_addr, length);
    } while (mapped == MAP_FAILED);
    return mapped;
}

static size_t estimated_max_heap_size(const void *heap_start) {
    void *const free_page_after_heap = map_free_pages_after(heap_start, 1);
    munmap(free_page_after_heap, 1);
    return ((uint8_t *) free_page_after_heap - (uint8_t *) HEAP_START);
}

/* Basic tests */

bool test_malloc1(va_list args) {
    const size_t chunk_size = va_arg(args, size_t);
    void *const ptr = _malloc(chunk_size);
    if (!test_mem_write_read(ptr, chunk_size)) return false;
    _free(ptr);
    return true;
}

bool test_free3_sequential(va_list args) {
    const size_t chunk_size = va_arg(args, size_t);
    void *const ptr1 = _malloc(chunk_size);
    void *const ptr2 = _malloc(chunk_size);
    void *const ptr3 = _malloc(chunk_size);
    if (!test_ptr3(ptr1, ptr2, ptr3)) return false;
    _free(ptr1);
    _free(ptr2);
    _free(ptr3);
    return true;
}

bool test_free3_middle_first(va_list args) {
    const size_t chunk_size = va_arg(args, size_t);
    void *const ptr1 = _malloc(chunk_size);
    void *const ptr2 = _malloc(chunk_size);
    void *const ptr3 = _malloc(chunk_size);
    if (!test_ptr3(ptr1, ptr2, ptr3)) return false;
    _free(ptr2);
    _free(ptr1);
    _free(ptr3);
    return true;
}

/* Allocation tests */

bool test_malloc2_with_allocation(va_list args) {
    const size_t first_chunk_size = va_arg(args, size_t);
    const size_t max_heap_size = estimated_max_heap_size(HEAP_START);
    const size_t overflow_chunk_size = max_heap_size - first_chunk_size;
    void *const ptr1 = _malloc(first_chunk_size);
    void *const ptr2 = _malloc(overflow_chunk_size);
    if (!test_mem2_write_read(ptr1, first_chunk_size, ptr2, overflow_chunk_size)) return false;
    _free(ptr1);
    _free(ptr2);
    return true;
}

bool test_malloc2_with_failproof_allocation(va_list args) {
    const size_t chunk_size = va_arg(args, size_t);
    const size_t mapping_size = va_arg(args, size_t);
    void *const ptr1 = _malloc(chunk_size);
    uint8_t *const mapped = map_free_pages_after(HEAP_START, mapping_size);
    const size_t max_heap_size = (uint8_t *) mapped - (uint8_t *) HEAP_START;
    void *const ptr2 = _malloc(max_heap_size);
    if (!test_mem_write_read(ptr1, chunk_size) ||
        !test_mem_write_read(mapped, mapping_size) ||
        !test_mem_write_read(ptr2, max_heap_size))
        return false;
    _free(ptr1);
    munmap(mapped, mapping_size);
    _free(ptr2);
    return true;
}
