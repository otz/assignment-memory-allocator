#define _DEFAULT_SOURCE

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <sys/mman.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(const struct block_header *block, size_t capacity_query) {
    return block->capacity.bytes >= capacity_query;
}

static size_t pages_count(size_t mem) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

static size_t round_pages(size_t mem) {
    return getpagesize() * pages_count(mem);
}

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t block_size_query) {
    return size_max(round_pages(block_size_query), REGION_MIN_SIZE);
}

extern inline bool region_is_invalid(const struct region *r);

static void *map_pages(const void *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

#define BLOCK_HEADER_SIZE offsetof(struct block_header, contents)
#define BLOCK_MIN_CAPACITY 24
#define BLOCK_MIN_SIZE (BLOCK_HEADER_SIZE + BLOCK_MIN_CAPACITY)

/* Allocates a memory region and initializes it with a block */
static struct region alloc_region(const void *addr, size_t storage_query) {
    const size_t capacity_query = size_max(BLOCK_MIN_CAPACITY, storage_query);
    const size_t region_size = region_actual_size(BLOCK_HEADER_SIZE + capacity_query);
    bool extend_status = true;
    void *mapped = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);
    if (mapped == MAP_FAILED) {
        extend_status = false;
        mapped = map_pages(addr, region_size, 0);
        if (mapped == MAP_FAILED) { return REGION_INVALID; }
    }
    const struct region region = (struct region) {
            .addr=mapped, .size = region_size, .extends = extend_status};
    block_init(mapped, (block_size) {.bytes = region_size}, NULL);
    return region;
}

static void *block_after(const struct block_header *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) { return NULL; }
    return region.addr;
}

/* --- Block splitting (if the found one is too large) --- */

static bool block_is_splittable(struct block_header *restrict block, size_t capacity_query) {
    return block->is_free && capacity_query + BLOCK_MIN_SIZE <= block->capacity.bytes;
}

static bool block_try_split(struct block_header *block, size_t storage_query) {
    const size_t capacity_query = size_max(BLOCK_MIN_CAPACITY, storage_query);
    if (block_is_splittable(block, capacity_query)) {
        const block_capacity occupied_block_capacity = (block_capacity) {.bytes = capacity_query};
        const block_size remaining_space_size = (block_size) {.bytes = (block->capacity.bytes - capacity_query)};
        block->capacity = occupied_block_capacity;
        struct block_header *const next_block_header = block_after(block);
        block_init(next_block_header, remaining_space_size, block->next);
        block->next = next_block_header;
        return true;
    }
    return false;
}

/* --- Merging adjacent free blocks --- */

static void *block_after(const struct block_header *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_are_continuous(const struct block_header *restrict fst, const struct block_header *restrict snd) {
    return (void *) snd == block_after(fst);
}

static bool blocks_are_mergeable(const struct block_header *restrict fst, const struct block_header *restrict snd) {
    return fst->is_free && snd->is_free && blocks_are_continuous(fst, snd);
}

static bool block_try_merge_with_next(struct block_header *block) {
    const struct block_header *const next_block = block->next;
    if (next_block == NULL || !blocks_are_mergeable(block, next_block)) { return false; }
    const block_size next_block_size = size_from_capacity(next_block->capacity);
    block->next = next_block->next;
    block->capacity.bytes += next_block_size.bytes;
    return true;
}

/* --- ...if the heap size is sufficient --- */

struct block_search_result {
    enum {
        BSR_FOUND, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};

static bool block_can_store_data(const struct block_header *block, size_t capacity_query) {
    return block->is_free && block_is_big_enough(block, capacity_query);
}

static struct block_search_result block_find_capable_to_store(struct block_header *restrict block, size_t capacity_query) {
    while (true) {
        if (block_try_merge_with_next(block)) { continue; }
        if (block_can_store_data(block, capacity_query)) { return (struct block_search_result) {BSR_FOUND, block}; }
        if (block->next == NULL) { break; }
        block = block->next;
    }
    return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, block};
}

/* Try to allocate memory on the heap starting at block without trying to expand the heap */
static struct block_search_result try_memalloc_existing(size_t storage_query, struct block_header *block) {
    const size_t capacity_query = size_max(BLOCK_MIN_CAPACITY, storage_query);
    const struct block_search_result search_result = block_find_capable_to_store(block, capacity_query);
    if (search_result.type == BSR_FOUND) {
        block_try_split(search_result.block, storage_query);
        search_result.block->is_free = false;
    }
    return search_result;
}

static struct block_header *grow_heap(struct block_header *restrict last, size_t storage_query) {
    const void *const alloc_addr = (void *) block_after(last);
    const struct region allocated_region = alloc_region(alloc_addr, storage_query);
    if (region_is_invalid(&allocated_region)) { return NULL; }
    struct block_header *allocated_block = (struct block_header *) allocated_region.addr;
    last->next = allocated_block;
    if (block_try_merge_with_next(last)) { return last; }
    return allocated_block;
}

/* Implements malloc logic and returns the header of the allocated block */
static struct block_header *memalloc(size_t storage_query, struct block_header *heap_start) {
    struct block_search_result search_result = try_memalloc_existing(storage_query, heap_start);
    if (search_result.type == BSR_CORRUPTED) { return NULL; }
    if (search_result.type == BSR_REACHED_END_NOT_FOUND) {
        if (grow_heap(search_result.block, storage_query) == NULL) { return NULL; }
        search_result = try_memalloc_existing(storage_query, search_result.block);
        if (search_result.type != BSR_FOUND) { return NULL; }
    }
    return search_result.block;
}

void *_malloc(size_t storage_query) {
    struct block_header *const block = memalloc(storage_query, (struct block_header *) HEAP_START);
    return (block != NULL) ? block->contents : NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) { return; }
    struct block_header *const header = block_get_header(mem);
    header->is_free = true;
    while (block_try_merge_with_next(header)) {}
}
