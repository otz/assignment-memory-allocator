#include "tests.h"

#define TEST(func, ...) run_test(stderr, #func, func, __VA_ARGS__)

int main(int argc, char *argv[]) {
    (void) argc; (void) argv;
    const size_t initial_allocation_query = 8;
    const size_t test_chunk_size = 8;

    heap_init(initial_allocation_query);
    TEST(test_malloc1, test_chunk_size);
    TEST(test_free3_middle_first, test_chunk_size);
    TEST(test_free3_sequential, test_chunk_size);
    TEST(test_malloc2_with_allocation, test_chunk_size);
    TEST(test_malloc2_with_failproof_allocation, test_chunk_size, test_chunk_size);
}
