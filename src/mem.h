#ifndef _MEM_H_
#define _MEM_H_

#include <stdio.h>

#define HEAP_START ((void*)0x04040000)

void *_malloc(size_t storage_query);

void _free(void *mem);

void *heap_init(size_t initial_size);

#define DEBUG_FIRST_BYTES 4

void debug_struct_info(FILE *f, const void *address);

void debug_heap(FILE *f, const void *ptr);

#endif
